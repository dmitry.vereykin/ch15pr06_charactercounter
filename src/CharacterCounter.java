/**
 * Created by Dmitry Vereykin on 7/29/2015.
 */
import java.util.Scanner;

public class CharacterCounter {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int count;

        System.out.print("Please enter the word to be used: ");
        String str = in.next();
        //String str = "Test string contains 3 bbb";
        char[] chars = str.toCharArray();

        System.out.print("Please enter the character be counted: ");
        char chosenCharacter = in.next().charAt(0);
        // char chosenCharacter = 'b';

        count = characterCount(chars, chosenCharacter, 0);

        System.out.print("\n\"" + str + "\"");
        System.out.print(" contains " + "\"" + chosenCharacter + "\" " + count);

        if (count == 1)
            System.out.print(" time");
        else
            System.out.print(" times");

    }

    public static int characterCount(char[] chars, char chosenChar, int pos) {
        int count;

        if (pos < chars.length) {
            count = characterCount(chars, chosenChar, pos + 1);

            if (chars[pos] == chosenChar)
                count++;
        } else {
            count = 0;
        }

        return count;
    }
}